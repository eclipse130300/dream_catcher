﻿Shader "Unlit/TransparentSilluhette"
{
    Properties
    {
        _MainColor ("Color", Color) = (1,1,1,1)
        _Alpha ("Alpha", Range(0.0, 1)) = 1
    }
    SubShader
    {
        Tags { "Queue" = "Transparent" }
        Zwrite off
        Blend SrcAlpha OneMinusSrcAlpha
        
        //if its not too many details
        Cull off

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float3 wNormal : TEXCOORD1;
                float4 vertexW : TEXCOORD2;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _Alpha;
            float4 _MainColor;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.wNormal = mul(unity_ObjectToWorld, v.normal);
                o.vertexW = mul(unity_ObjectToWorld, v.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float3 viewDir = normalize(_WorldSpaceCameraPos - i.vertexW.rgb);
                float NdotView = dot(i.wNormal, viewDir);

                //squared
                float silhouette = (_Alpha/NdotView) * (_Alpha/NdotView);

                //simple
                //float silhouette = _Alpha/NdotView;
                
                float newOpacity = min(1, silhouette);
                
                // sample the texture
                fixed4 col = _MainColor;

                col.a = newOpacity;
                return col;
            }
            ENDCG
        }
    }
}
