﻿#define H 255

void GameBoyColor_float(in float3 col, out float3 Out)
{
    float colValue = (float)(col.r + col.g + col.b) / 3;

    half3 col1 = half3(202./H, 200./H, 159./H);
    half3 col2 = half3(139./H, 172./H, 15./H);
    half3 col3 = half3(48./H, 98./H,48./H);
    half3 col4 = half3(15./H, 56./H, 15./H);
    
    // if(colValue > 0.75)
    // {
    //     float lerpVal = (colValue - 0.75) / 1 - 0.75;
    //     col = lerp(col2, col1, lerpVal);
    // }
    // else if(colValue < 0.75 && colValue > 0.5)
    // {
    //     float lerpVal = (colValue - 0.5) / 0.75 - 0.5;
    //     col = lerp(col3, col2, lerpVal);
    // }
    // else if(colValue > 0.25 && colValue < 0.5)
    // {
    //     float lerpVal = (colValue - 0.25) / 0.5 - 0.25;
    //     col = lerp(col4, col3, lerpVal);
    // }
    // else
    // {
    //     col = col4;
    // }

    if(col.r > 0.75)
    {
        col = col1;
    }
    else if(col.r < 0.75 && col.r > 0.5)
    {
        col = col2;
    }
    else if(col.r > 0.25 && col.r < 0.5)
    {
        col = col3;
    }
    else
    {
        col = col4;
    }

    Out = col;
}
