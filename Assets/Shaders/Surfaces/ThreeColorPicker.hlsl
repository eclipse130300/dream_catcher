void Pick_float(float val, float3 col1, float3 col2, float3 col3, out float3 col)
{
    if(val < 0.3)
    {
        col = col1;
    }

    if(val >= 0.3 && val <= 0.9)
    {
        col = col2;
    }

    if(val > 0.9)
    {
        col = col3;
    }
        
}