// Inspector Gadgets // https://kybernetik.com.au/inspector-gadgets // Copyright 2021 Kybernetik //

namespace InspectorGadgets
{
    /// <summary>Represents a Unity Editor state which can be used as a condition.</summary>
    public enum EditorState
    {
        /// <summary>All the time, regardless of the current state of the Unity Editor.</summary>
        Always,

        /// <summary>When the Unity Editor is in Play Mode.</summary>
        Playing,

        /// <summary>When the Unity Editor is not in Play Mode.</summary>
        Editing,
    }

    public static partial class IGUtils
    {
        /************************************************************************************************************************/

        /// <summary>Returns true if the Unity Editor is currently in the specified `state`.</summary>
        public static bool IsNow(this EditorState state)
        {
            switch (state)
            {
#if UNITY_EDITOR
                case EditorState.Playing:
                    return UnityEditor.EditorApplication.isPlaying;
                case EditorState.Editing:
                    return !UnityEditor.EditorApplication.isPlaying;
#else
                case EditorState.Playing:
                    return true;
                case EditorState.Editing:
                    return false;
#endif
                case EditorState.Always:
                default:
                    return true;
            }
        }

        /************************************************************************************************************************/
    }
}

