﻿using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace.Scenes;
using DefaultNamespace.Utils;
using EventBusSystem;
using EventBusSystem.Handlers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CheatPanelUI : MonoBehaviour
{
    [SerializeField] private TMP_InputField _inputField;
    [SerializeField] private Button _closeButton;
    [SerializeField] private Button _loadLevelButton;
    [SerializeField] private Button _winButton;
    [SerializeField] private Button _loseButton;

    private SceneCoroutineLevelLoader _levelLoader;

    private void Awake()
    {
        Time.timeScale = 0;

        _levelLoader = FindObjectOfType<SceneCoroutineLevelLoader>();
        
        _closeButton.onClick.AddListener(Close);
        _loadLevelButton.onClick.AddListener(OnLoadLevelButtonClick);
        _winButton.onClick.AddListener(OnWinLevelButtonClick);
        _loseButton.onClick.AddListener(OnLoseLevelButtonClick);
    }

    private void OnWinLevelButtonClick()
    {
        EventBus.RaiseEvent<IGameEndHandler>(h => h.OnGameEnd(true));
        Close();
    }

    private void OnLoseLevelButtonClick()
    {
        EventBus.RaiseEvent<IGameEndHandler>(h => h.OnGameEnd(false));
        Close();
    }

    private void OnLoadLevelButtonClick()
    {
        var sceneToLoad = _inputField.text;
        
        if (SceneUtils.SceneIsInBuildSettings(sceneToLoad))
        {
            GameSceneLoader.Instance.LoadGameLevel(sceneToLoad);
        }
    }

    private void Close()
    {
        if (SceneUtils.SceneIsInBuildSettings("CheatPanel"))
        {
            _levelLoader.UnloadScene("CheatPanel");
        }
    }

    private void OnDestroy()
    {
        _closeButton.onClick.RemoveAllListeners();
        _loadLevelButton.onClick.RemoveAllListeners();
        _winButton.onClick.RemoveAllListeners();
        _loseButton.onClick.RemoveAllListeners();
        
        Time.timeScale = 1;
    }
}
