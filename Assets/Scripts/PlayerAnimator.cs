﻿using EventBusSystem;
using EventBusSystem.Handlers;
using UnityEngine;

public class PlayerAnimator : MonoBehaviour
{
    private Animator _animator;

    private void Start()
    {
        _animator = GetComponentInChildren<Animator>();
    }

    public void PlayerRunAnimation()
    {
        _animator?.SetBool("IsRun", true);
    }

    public void PlayIdleAnimation()
    {
        _animator?.SetBool("IsRun", false);
    }
}
