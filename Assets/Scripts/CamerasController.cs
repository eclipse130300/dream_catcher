﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using EventBusSystem;
using EventBusSystem.Handlers;
using Player;
using UnityEngine;

public class CamerasController : MonoBehaviour, IRunSceneLoadedHandler
{
    [SerializeField] private CinemachineVirtualCamera _camera;
    private void Awake()
    {
        EventBus.Subscribe(this);
    }

    private void OnDestroy()
    {
        EventBus.Unsubscribe(this);
    }

    public void OnRunSceneLoaded()
    {
        var player = GameObject.FindGameObjectWithTag("Player");
        
        if(player)
            _camera.Follow = player.transform;
        else
            Debug.LogWarning("No player found for camera to follow!");
    }
}
