﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetTargetPositionAtTheCenterOfMeshRenderers : MonoBehaviour
{
    [SerializeField] private Renderer[] _renderers;

    [SerializeField] private float _maxOffset = 1f;

    private void Awake()
    {
        if (_renderers.Length == 0)
        {
            Debug.LogError("Set renderers for eye target for calculating position.You should probably use monster eyes?");
            return;
        }
    }

    private void Update()
    {
        if (_renderers.Length == 0)
        {
            return;
        }

        //use local pos if we want to use hierarchy?
        var meshCenter = CalculateCentralMeshRendererPos();
        var transformCenter = CalculateCentralTransform();
        var direction = (meshCenter - transformCenter).normalized;
        
        Debug.DrawLine(transformCenter, transformCenter + direction * _maxOffset, Color.red);

        var toMeshCenter = meshCenter - transformCenter;

        transform.position = transformCenter + Vector3.ClampMagnitude(toMeshCenter, _maxOffset);
    }

    private Vector3 CalculateCentralMeshRendererPos()
    {
        Vector3 center = Vector3.zero;
        foreach (var renderer in _renderers)
        {
            center += renderer.bounds.center;
        }

        center /= _renderers.Length;

        return center;
    }

    private Vector3 CalculateCentralTransform()
    {
        Vector3 center = Vector3.zero;
        foreach (var renderer in _renderers)
        {
            center += renderer.transform.position;
        }
        
        center /= _renderers.Length;
        return center;
    }
}
