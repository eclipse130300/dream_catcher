﻿using System;
using UnityEngine;

namespace DefaultNamespace.Utils
{
    public class DrawCube : MonoBehaviour
    {
        [SerializeField] private Color _color = Color.red;
        [SerializeField] private Vector2 _size = Vector2.one;

        [SerializeField] private bool _usePhysicsSize = false;
        [SerializeField] private BoxCollider2D _physicsCollider;
        
        #if UNITY_EDITOR

        private void OnValidate()
        {
            if (_usePhysicsSize && _physicsCollider)
            {
                _size.x = _physicsCollider.size.x * transform.localScale.x;
                _size.y = _physicsCollider.size.y * transform.localScale.y;
            }
        }

#endif
        
        private void OnDrawGizmos()
        {
            Gizmos.color = _color;
            Gizmos.DrawCube(transform.position, _size);
        }
    }
}