﻿#if UNITY_EDITOR

using UnityEngine;

[ExecuteInEditMode]
public class SetSpriteWidthOfSpriteRenderer : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _myRenderer;
    [SerializeField] private SpriteRenderer _targetRenderer;

    private void Update()
    {
        var mySize = _myRenderer.size;
        mySize.x = _targetRenderer.size.x;

        _myRenderer.size = mySize;
    }
}

#endif
