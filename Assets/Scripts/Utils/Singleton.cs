﻿using UnityEngine;

namespace DefaultNamespace.Utils
{
    public class Singleton<T> : MonoBehaviour where T : Component 
    {
        private static T _instance;

        public static T Instance
        {
            get
            {
                if (!_instance)
                {
                    _instance = FindObjectOfType<T>();
                }

                return _instance;
            }
        }
    }
}