﻿using UnityEditor;
using UnityEngine;

namespace DefaultNamespace.Utils
{
    public static class SceneUtils
    {
        public static bool SceneIsInBuildSettings(string sceneName)
        {
#if UNITY_EDITOR
            SceneAsset sceneObject = GetSceneObject(sceneName);

            if (sceneObject != null)
            {
                return true;
            }

            return false;
#endif

            return true;
        }

#if UNITY_EDITOR
        private static SceneAsset GetSceneObject(string sceneObjectName) {
            if (string.IsNullOrEmpty(sceneObjectName)) {
                // Early exit as know it will return null when string null or empty.
                return null;
            }
            // Iterate over the scenes in the build settings
            foreach (EditorBuildSettingsScene editorScene in EditorBuildSettings.scenes) {
                // We found the scene object's name in the editor scene's path.
                // This assumes that a scene will not be named exactly the same as a parent folder & a duplicate of another scene.
                if (editorScene.path.IndexOf(sceneObjectName) != -1) {
                    return AssetDatabase.LoadAssetAtPath(editorScene.path, typeof(SceneAsset)) as SceneAsset;
                }
            }
            // Scene not found, assume it isn't in the build settings.
            Debug.LogWarning("Scene [" + sceneObjectName + "] cannot be used. Add this scene to the 'Scenes in the Build' in build settings.");
            return null;
        }
#endif
    }
}