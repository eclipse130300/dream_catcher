﻿using UnityEngine;

namespace DefaultNamespace.Utils
{
    public static class MyExtensions
    {
        public static void SetLocalY(this Transform transform, float value)
        {
            var pos = transform.localPosition;
            pos.y = value;
            transform.localPosition = pos;
        }
    }
}