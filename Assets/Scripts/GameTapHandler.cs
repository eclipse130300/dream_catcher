﻿using System;
using DefaultNamespace.Utils;
using EventBusSystem;
using EventBusSystem.Handlers;
using UnityEngine;

//todo: what the hell is this? Why don't put it just on the start scene? REWORK!
public class GameTapHandler : Singleton<GameTapHandler>
{
    private bool _isActive;

    public event Action OnTap;
    public event Action OnTapRelease;

    public void Awake()
    {
        _isActive = true;
    }

    private void Update()
    {
        if(_isActive)
            ListenButton();
    }

    private void ListenButton()
    {
        if (Input.GetMouseButtonDown(0))
        {
            EventBus.RaiseEvent<ITapHandler>(h => h.OnTap());
            OnTap?.Invoke();
        }
        if (Input.GetMouseButtonUp(0))
        {
            EventBus.RaiseEvent<ITapReleaseHandler>(h => h.OnTapRelease());
            OnTapRelease?.Invoke();
        }
    }

    public void Activate()
    {
        _isActive = true;
    }

    public void Deactivate()
    {
        _isActive = false;
    }
 }

