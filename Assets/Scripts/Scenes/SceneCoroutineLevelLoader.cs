﻿using System;
using System.Collections;
using System.Collections.Generic;
using EventBusSystem;
using EventBusSystem.Handlers;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DefaultNamespace.Scenes
{
    public class SceneCoroutineLevelLoader : MonoBehaviour
    {
        private Scene _scene;
        private State _state = State.Ready;
        
        private List<string> _loadedScenes = new List<string>();

        public bool SceneIsLoaded(string sceneName)
        {
            if (_loadedScenes.Contains(sceneName))
            {
                return true;
            }

            return false;
        }

        public void LoadSceneAdditive(string sceneName)
        {
            if (!_loadedScenes.Contains(sceneName))
            {
                StartCoroutine(
                    LoadSceneAsyncAdd(sceneName, () =>
                {
                    _loadedScenes.Add(sceneName);
                    EventBus.RaiseEvent<IDialogeSceneLoadedHandler>(h => h.OnDialogeSceneLoaded());
                })
                    );
            }
        }

        public void UnloadScene(string sceneName)
        {
            if (_loadedScenes.Contains(sceneName))
            {
                StartCoroutine(
                    UnloadSceneAsyncAdd(sceneName, () =>
                    {
                        _loadedScenes.Remove(sceneName);
                    }));
            }
        }
        
        public void LoadScene(string sceneName)
        {
            _loadedScenes.Clear();
            
            if (!_loadedScenes.Contains(sceneName))
            {
                StartCoroutine(
                    LoadSceneAsyncSingle(sceneName, () =>
                    {
                        _loadedScenes.Add(sceneName);
                        EventBus.RaiseEvent<IRunSceneLoadedHandler>(h => h.OnRunSceneLoaded());
                    })
                );
            }
        }
        
        private IEnumerator UnloadSceneAsyncAdd(string scene, Action callback = null)
        {
            var unload = SceneManager.UnloadSceneAsync(scene);

            while (unload.isDone)
            {
                yield return null;
            }

            callback?.Invoke();
        }

        private IEnumerator LoadSceneAsyncAdd(string scene, Action callback = null)
        {
            //we are loading dialogue scenes here...so it is dialogue scenes for now!
            var load = SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);

            while (load.isDone)
            {
                yield return null;
            }

            callback?.Invoke();
        }
        
        private IEnumerator LoadSceneAsyncSingle(string scene, Action callback = null)
        {
            //we are loading game scenes here...so it is game scenes for now!
            var load = SceneManager.LoadSceneAsync(scene, LoadSceneMode.Single);
            
            yield return new WaitUntil(() => load.isDone);
            
            callback?.Invoke();
        }

        public enum State
        {
            Ready = 0,
            Loading,
            Unloading,
        }
    }
}