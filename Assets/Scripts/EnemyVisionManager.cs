﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Enemy;
using UnityEngine;

public class EnemyVisionManager : MonoBehaviour
{
    private List<VisionAnimator> _eyeVisionAnimators = new List<VisionAnimator>();

    private void Awake()
    {
        _eyeVisionAnimators = GetComponentsInChildren<VisionAnimator>().ToList();
    }

    public void TriggerEnemyVision(EnemyFovInfo info, Action callback = null)
    {
        //we start all vision sequences,
        //but only first one with callback...for preventing bugs
        _eyeVisionAnimators[0].AnimateVision(info, callback);

        if (_eyeVisionAnimators.Count > 1)
        {
            for (int i = 1; i < _eyeVisionAnimators.Count; i++)
            {
                _eyeVisionAnimators[i].AnimateVision(info);
            }
        }
    }

    public void RewindEnemyVision(float time, Action callback = null)
    {
        _eyeVisionAnimators[0].RewindVision(time, callback);

        if (_eyeVisionAnimators.Count > 1)
        {
            for (int i = 1; i < _eyeVisionAnimators.Count; i++)
            {
                _eyeVisionAnimators[i].RewindVision(time);
            }
        }
    }
}
