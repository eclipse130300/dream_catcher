﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Narrative
{
    public class DialogueGameNode : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _dialogueText;
        [SerializeField] private Image _dialogueCharImage;
        [SerializeField] private Image _backgroundImage;
        public void SetupNode(VisualNodeData visualNodeData)
        {
            _dialogueCharImage.sprite = visualNodeData.Sprite;
            _dialogueText.text = visualNodeData.NodeText;
            
            TryApplyColor(visualNodeData.ColorHex);
        }

        private void TryApplyColor(string hexCol)
        {
            if (ColorUtility.TryParseHtmlString(hexCol, out var color))
            {
                _backgroundImage.color = color;
            }
        }
    }
}