﻿using UnityEngine;

namespace Narrative
{
    public class VisualNodeData
    {
        public Sprite Sprite;
        public string CharName;
        public string NodeText;
        public string ColorHex;
    }
}