﻿using System;
using DG.Tweening;
using EventBusSystem;
using EventBusSystem.Handlers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Narrative
{
    public class PlayerAnswerNode : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _answerText;
        [SerializeField] private Button _button;

        private int _index;
        private string _nextSceneName;

        private void Awake()
        {
            _button.onClick.AddListener(OnOptionButtonClick);
        }

        public void SetupAnswer(string answer, int index, string nextGameSceneName = "")
        {
            _answerText.text = answer;
            _index = index;
            _nextSceneName = nextGameSceneName;
        }

        private void OnOptionButtonClick()
        {
            EventBus.RaiseEvent<IPlayerPickedUpAnswer>(h => h.OnPlayerPickedUpAnswer(_answerText.text, _index, _nextSceneName));
        }
    }
}