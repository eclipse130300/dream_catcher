﻿using System;
using DG.Tweening;
using EventBusSystem;
using EventBusSystem.Handlers;
using EventBusSystem.Handlers.UI;
using Narrative;
using UnityEngine;
using VIDE_Data;

public class NarrationUIManager : MonoBehaviour, IPlayerPickedUpAnswer
{
    [SerializeField] private Transform _dialoguesParent;
    [SerializeField] private Transform _playerAnswersParent;

    [SerializeField] private GameObject _npcNodeDialoguePrefab;
    [SerializeField] private GameObject _playerNodeDialoguePrefab;
    [SerializeField] private GameObject _answerNodePrefab;

    [SerializeField] private GameObject _playButtonGameObject;

    private VIDE_Assign _dialogue;
    private VD.NodeData _currentNodeData;

    private void Awake()
    {
        EventBus.Subscribe(this);
        //VD.LoadState(SceneManager.GetActiveScene().name, true);
        _dialogue = GetComponent<VIDE_Assign>();
        Begin(_dialogue);
        
        _playButtonGameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        EventBus.Unsubscribe(this);
    }

    void Begin(VIDE_Assign dialogue)
    {
        FullCleanUp();
        
        VD.OnNodeChange += UpdateUI;
        VD.OnEnd += EndDialogue;

        VD.BeginDialogue(dialogue); //Begins dialogue, will call the first OnNodeChange
    }

    private void EndDialogue(VD.NodeData data)
    {
        VD.OnNodeChange -= UpdateUI;
        VD.OnEnd -= EndDialogue;
        VD.EndDialogue();

        ActivatePlayButton();
        //VD.SaveState(SceneManager.GetActiveScene().name, true); //Saves VIDE stuff related to EVs and override start nodes
    }

    private void UpdateUI(VD.NodeData data)
    {
        _currentNodeData = data;
        CleanupAnswers();

        //If this new Node is a Player Node, set the player choices offered by the node
        if (data.isPlayer)
        {
            //isPlayer text
            //Debug.Log("IsPlayer");

            if (_currentNodeData.comments.Length > 1)
            {
                SetAnswers(data);
            }
            else if (_currentNodeData.comments.Length == 1)
            {
                var playerVisualData = _currentNodeData.GetBasePlayerNodeData();
                
                playerVisualData.NodeText = _currentNodeData.comments[0];
                
                SpawnPlayerDialogueNode(playerVisualData);
            }
            
        }
        else 
        {
            //is npcText
            //Debug.Log("IsNpc");

            var npcVisualData = _currentNodeData.GetNpcNodeData();

            if (HasColor(_currentNodeData))
            {
                npcVisualData.ColorHex = GetColor(_currentNodeData);
            }

            //TODO spawn npc node with sprite and name
            SpawnNpcDialogueNode(npcVisualData);
        }
    }

    //move it to different class?
    private void GenerateEventByTag(string tag)
    {
        if (tag.Contains("player"))
        {
            EventBus.RaiseEvent<IPlayerSpeaksHandler>(h => h.OnPlayerSpeak());
        }
        else if (tag.Contains("narrator"))
        {
            EventBus.RaiseEvent<INarratorSpeaksHandler>(h => h.OnNarratorSpeak());
        }
    }

    private bool HasColor(VD.NodeData nodeData)
    {
        return nodeData.extraVars.ContainsKey("Color");
    }

    private string GetColor(VD.NodeData nodeData)
    {
        return (string)nodeData.extraVars["Color"];
    }

    private void ActivatePlayButton()
    {
        _playButtonGameObject.SetActive(true);
    }

    private void SpawnPlayerDialogueNode(VisualNodeData visualNodeData)
    {
        var playerNode = Instantiate(_playerNodeDialoguePrefab, Vector3.zero, Quaternion.identity);
        SetupNode(playerNode, visualNodeData);
        
        GenerateEventByTag(_currentNodeData.tag);
    }

    private void SpawnNpcDialogueNode(VisualNodeData visualNodeData)
    {
        var npcNode = Instantiate(_npcNodeDialoguePrefab, Vector3.zero, Quaternion.identity);
        SetupNode(npcNode, visualNodeData);
        
        GenerateEventByTag(_currentNodeData.tag);
    }

    private void SetupNode(GameObject newNode, VisualNodeData visualNodeData)
    {
        newNode.transform.SetParent(_dialoguesParent);
        
        var node = newNode.GetComponent<DialogueGameNode>();
        node.SetupNode(visualNodeData);
    }

    private void SetAnswers(VD.NodeData data)
    {
        for (int i = 0; i < data.comments.Length; i++)
        {
            //spawn new player answer
            var answer = Instantiate(_answerNodePrefab, Vector3.zero, Quaternion.identity);
            answer.transform.SetParent(_playerAnswersParent);
            //and set answer text there
            var playerAnswerNode = answer.GetComponent<PlayerAnswerNode>();

            string sceneName = "";
            //check if it has a next sceneName to load
            if (!String.IsNullOrWhiteSpace(data.extraData[i]))
            {
                sceneName = data.extraData[i];
            }

            playerAnswerNode.SetupAnswer(data.comments[i], i, sceneName);
        }
    }


    private void FullCleanUp()
    {
        foreach (Transform child in _dialoguesParent)
        {
            Destroy(child.gameObject);
        }
        CleanupAnswers();
    }

    private void CleanupAnswers()
    {
        foreach (Transform child in _playerAnswersParent)
        {
            Destroy(child.gameObject);
        }
    }

    public void OnPlayerPickedUpAnswer(string answer, int index, string nextGameSceneName = "")
    {
        if (!String.IsNullOrWhiteSpace(nextGameSceneName))
        {
            GameSceneLoader.Instance.SetNextGameSceneName(nextGameSceneName);
        }
        
        VisualNodeData visualNodeData = _currentNodeData.GetBasePlayerNodeData();
        visualNodeData.NodeText = answer;
        
        CleanupAnswers();

        VD.nodeData.commentIndex = index;

        //TODO: save sprite and name, untill we pick something, after pick we spawn player dialogue
        Sequence sequence = DOTween.Sequence();
        sequence
            .AppendCallback((() =>
            {
                SpawnPlayerDialogueNode(visualNodeData);
            }))
            .AppendInterval(1f)
            .OnComplete((() =>
            {
                VD.Next();
            }));
    }

    //TEST!DELETE LATER!
    private void Update()
    {
        if (VD.isActive && !_currentNodeData.isPlayer && !_currentNodeData.isEnd)
        {
            WaitForLoadNext();
        }

        else if (VD.isActive && _currentNodeData.isPlayer && !_currentNodeData.isEnd)
        {
            if (_currentNodeData.comments.Length == 1)
            {
                WaitForLoadNext();
            }
        }

        void WaitForLoadNext()
        {
            if (Input.GetMouseButtonDown(0))
            {
                VD.Next();
            }
        }
        
        
    }
}
