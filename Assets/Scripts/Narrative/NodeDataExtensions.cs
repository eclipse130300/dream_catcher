﻿using GameSetting;
using VIDE_Data;

namespace Narrative
{
    //kinda visualNodeData factory?
    public static class NodeDataExtensions
    {
        public static VisualNodeData GetBasePlayerNodeData(this VD.NodeData data)
        {
            VisualNodeData visualNodeData = new VisualNodeData();
            
            //Set node sprite if there's any, otherwise try to use default sprite
            if (data.sprite != null)
                visualNodeData.Sprite = data.sprite;
            else if (VD.assigned.defaultPlayerSprite != null)
                visualNodeData.Sprite = VD.assigned.defaultPlayerSprite;

            

            //If it has a tag, show it, otherwise let's use the alias we set in the VIDE Assign
            if (data.tag.Length > 0)
                visualNodeData.CharName = data.tag;
            else
                visualNodeData.CharName = GameSettings.Instance.PlayerDefaultName;

            return visualNodeData;
        }

        public static VisualNodeData GetNpcNodeData(this VD.NodeData data)
        {
            VisualNodeData visualNodeData = new VisualNodeData();
            
            string text = data.comments[data.commentIndex];
            
            //Set node sprite if there's any, otherwise try to use default sprite
            if (data.sprite != null)
            {
                
                //For NPC sprite, we'll first check if there's any "sprite" key
                //Such key is being used to apply the sprite only when at a certain comment index
                if (data.extraVars.ContainsKey("sprite"))
                {
                    if (data.commentIndex == (int)data.extraVars["sprite"])
                        visualNodeData.Sprite = data.sprite;
                    else
                        visualNodeData.Sprite = VD.assigned.defaultNPCSprite; //If not there yet, set default dialogue sprite
                }
                else //Otherwise use the node sprites
                {
                    visualNodeData.Sprite = data.sprite;
                }
            } //or use the default sprite if there isnt a node sprite at all
            else if (VD.assigned.defaultNPCSprite != null)
                visualNodeData.Sprite = VD.assigned.defaultNPCSprite;

            //If it has a tag, show it, otherwise let's use the alias we set in the VIDE Assign
            if (data.tag.Length > 0)
                visualNodeData.CharName = data.tag;
            else
                visualNodeData.CharName = VD.assigned.alias;

            visualNodeData.NodeText = text;

            return visualNodeData;
        }
    }
}