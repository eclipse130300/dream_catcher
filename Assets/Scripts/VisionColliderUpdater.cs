﻿using UnityEngine;

public class VisionColliderUpdater : MonoBehaviour
{
    private MeshCollider _meshCollider;

    private void Awake()
    {
        _meshCollider = GetComponent<MeshCollider>();
    }

    public void UpdateMeshCollider(Mesh mesh)
    {
        _meshCollider.sharedMesh = null; //we set an other mesh
        _meshCollider.sharedMesh = mesh; // we set again customMesh and the collider is going to update
    }
}
