﻿using UnityEngine;

namespace ParralaxEffect
{
    public class CameraFollowTarget : MonoBehaviour
    {
        public GameObject target;
        public float PixelsPerUnit;
        [SerializeField] private float zOffset = 10f;
 
        void Update()
        {
            var targetPos = target.transform.position + Vector3.back * zOffset;
            transform.position = PixelPerfectClamp(targetPos, PixelsPerUnit);
        }
 
        private Vector3 PixelPerfectClamp(Vector3 moveVector, float pixelsPerUnit)
        {
            Vector3 vectorInPixels = new Vector3(Mathf.CeilToInt(moveVector.x * pixelsPerUnit), Mathf.CeilToInt(moveVector.y * pixelsPerUnit), Mathf.CeilToInt(moveVector.z * pixelsPerUnit));                                         
            return vectorInPixels / pixelsPerUnit;
        }
    }
}