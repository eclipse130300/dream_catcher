﻿using UnityEngine;

namespace ParralaxEffect
{
    public class Parallax : MonoBehaviour
    {
        private const float LerpSpeed = 10;
        
        private float _length, _startPos;
        private Camera _cam;
        
        [Range(0,1f)]
        [SerializeField] private float _parallaxEffect;

        private void Start()
        {
            _startPos = transform.position.x;
            _length = GetComponent<SpriteRenderer>().bounds.size.x;
            
            _cam = Camera.main;
        }

        private void LateUpdate()
        {
            float temp = (_cam.transform.position.x * (1 - _parallaxEffect));
            float dist = (_cam.transform.position.x * _parallaxEffect);

            //var currentPos = transform.position;
            var targetPos = new Vector3(_startPos + dist, transform.position.y, transform.position.z);

            transform.position = targetPos;

            //transform.position = Vector3.Lerp(currentPos, targetPos, LerpSpeed * Time.deltaTime);

            if (temp > _startPos + _length)
                _startPos += _length;
            else if (temp < _startPos - _length)
                _startPos -= _length;
        }
    }
}