﻿using UnityEngine;

namespace ParralaxEffect
{
    public class ParallaxEndlessRunner : MonoBehaviour
    {
        [SerializeField] private GameObject _movingObject;
        [Range(0,1)]
        [SerializeField] private float _parallaxEffect;

        [SerializeField] private bool _reversed = false;
    
        private float _length;
        private float _startPos;
        private float _sign = 1f;

        private float _offset;

        private void Start()
        {
            _startPos = transform.position.x;
            _length = GetComponent<SpriteRenderer>().bounds.size.x;

            if (_reversed)
            {
                _sign = -1f;
            }
        }

        private void Update()
        {
            var temp = _sign * (_movingObject.transform.position.x ) * (_parallaxEffect);
            var dist = _movingObject.transform.position.x * _parallaxEffect;
        
            transform.position = new Vector3(_startPos + dist, transform.position.y, transform.position.z);

            if (temp > _startPos + _length)
            {
                _startPos += _length;
            }

        
            else if (temp < _startPos - _length)
            {
                _startPos -= _length;
            }
        }
    }
}
