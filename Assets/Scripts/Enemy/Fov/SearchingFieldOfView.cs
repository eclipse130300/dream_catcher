﻿using System;
using GameSetting;
using UnityEngine;

namespace Enemy.Fov
{
    public class SearchingFieldOfView : MonoBehaviour
    {
        [SerializeField] private LayerMask _raycastLayerMask;
        [SerializeField] private LayerMask _searchingLayerMask;
        public float VisionDistance { get; set; }

        public event Action OnTargetDetected;
        private const string PlayerLayer = "Player";

        //private LayerMask _layerMask;
        private Mesh _mesh;
        private Vector3 _origin;
    
        private bool _isSearchingTarget;
    
        private float _fov = 0f;

        public float Inclination { get; set; }


        public void SetupRaycast(float fovAngle, float inclination)
        {
            _fov = fovAngle;
            Inclination = inclination;
        }

        private void Awake()
        {
            ActivateTargetSearch();
        }

        void Start()
        {
            _mesh = new Mesh();
            GetComponent<MeshFilter>().mesh = _mesh;
            _origin = Vector3.zero;
        }

        private void Update()
        {
            _origin = transform.position;
        }

        public void ActivateTargetSearch()
        {
            _isSearchingTarget = true;
        }

        public void DeactivateTargetSearch()
        {
            _isSearchingTarget = false;
        }

        public void LateUpdate()
        {
            int rayCount = GameSettings.Instance.EyeRaycount;
            float angle = (-90f + Inclination) + (_fov/2); //-90 to look down
            float angleIncrease = _fov / rayCount;

            Vector3[] vecticies = new Vector3[rayCount + 1 + 1];
            Vector2[] uv = new Vector2[vecticies.Length];
            int[] triangles = new int[rayCount * 3];

            vecticies[0] = Vector3.zero;

            int vertexIndex = 1;
            int triangleIndex = 0;
            for (int i = 0; i <= rayCount; i++)
            {
                Vector3 vertex;           

                RaycastHit2D raycastHit2D =
                    Physics2D.Raycast(_origin, GetVectorFromAngle(angle), VisionDistance, _raycastLayerMask);

                if (raycastHit2D.collider == null)
                {
                    vertex = GetVertexThrough(angle);
                }
                else
                {
                    int collidingObjLayer = 1 << raycastHit2D.collider.gameObject.layer;
                    if ((collidingObjLayer & _searchingLayerMask) != 0)
                    {
                        vertex = GetVertexThrough(angle);

                        if (_isSearchingTarget)
                        {
                            OnTargetDetected?.Invoke();
                            //Debug.Log("Player detected!");
                            //if we search only for once!
                            DeactivateTargetSearch();
                        }
                    }
                    else
                    {
                        vertex = raycastHit2D.point;
                    }
                    // int playerLayer = LayerMask.NameToLayer(LayerNames.PlayerLayer);
                }

                vertex -= _origin;

                vecticies[vertexIndex] = vertex;
            
                if (i > 0)
                {
                    triangles[triangleIndex + 0] = 0;
                    triangles[triangleIndex + 1] = vertexIndex -1;
                    triangles[triangleIndex + 2] = vertexIndex;

                    triangleIndex += 3;
                }
            
                vertexIndex++;
                angle -= angleIncrease; //for clockwise angle increase to properly render polygon
            }

            _mesh.vertices = vecticies;
            _mesh.uv = uv;
            _mesh.triangles = triangles;
        
            _mesh.RecalculateBounds();
            //_visionColliderUpdater.UpdateMeshCollider(_mesh);
        }

        private Vector3 GetVertexThrough(float angle)
        {
            float angleDown = Vector2.Angle(Vector2.down, GetVectorFromAngle(angle));
            float hypotenuse = VisionDistance / Mathf.Cos(angleDown * Mathf.Deg2Rad);
            return _origin + GetVectorFromAngle(angle) * hypotenuse;
        }

        private Vector3 GetVectorFromAngle(float angle)
        {
            float radAngle = angle * (Mathf.PI/180f);
            return new Vector3(Mathf.Cos(radAngle), Mathf.Sin(radAngle));
        }
    }
}
