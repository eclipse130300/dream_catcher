﻿using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Enemy.Fov;
using EventBusSystem;
using EventBusSystem.Handlers;
using GameSetting;
using UnityEngine;

namespace Enemy
{
    public class EnemyController : MonoBehaviour, IEnemyTriggeredHandler, IGameEndHandler
    {
        [SerializeField] private GameObject[] eyes;
    
        [Space(10)] [Header("Enemy overall settings")] 
        [SerializeField] private float _warningTime = 0.1f;
        [Space(10)] [SerializeField] private bool _activateSearchingOnAwake;
    
        //private Color _startEnemyColor;

        private bool _gameIsOver;
        private EnemyAnimatorWrapper _enemyAnimatorWrapper;

        //overthink later with two eyes?
        private List<SearchingFieldOfView> _eyesFovs = new List<SearchingFieldOfView>();
        private List<ParticlesTrigger> _eyeParticles = new List<ParticlesTrigger>();

        private EnemyVisionManager _enemyVisionManager;
        private Sequence _enemyAnimationSequence;

        private void Awake()
        {
            EventBus.Subscribe(this);
        
            //_startEnemyColor = _enemySpriteRenderer.color;
        
            //get components
            foreach (var eye in eyes)
            {
                _eyesFovs.Add(eye.GetComponent<SearchingFieldOfView>());
            }

            _eyeParticles = GetComponentsInChildren<ParticlesTrigger>().ToList();
            _enemyAnimatorWrapper = GetComponent<EnemyAnimatorWrapper>();
            _enemyVisionManager = GetComponent<EnemyVisionManager>();

            if (_activateSearchingOnAwake)
            {
                foreach (var eyeFov in _eyesFovs)
                {
                    eyeFov.ActivateTargetSearch();
                    eyeFov.OnTargetDetected += OnPlayerFound;
                }
            }
            //_warningSpriteRenderer.gameObject.SetActive(false);
        }

        private void OnDestroy()
        {
            StopDetectingPlayer();
        
            _enemyAnimationSequence.Kill(this);
        }

        private void OnPlayerFound()
        {
            foreach (var eyeFov in _eyesFovs)
            {
                eyeFov.DeactivateTargetSearch();
                eyeFov.OnTargetDetected -= OnPlayerFound;
            }
            
            //Debug.Log("on player found");
            
            EventBus.RaiseEvent<IGameEndHandler>(h => h.OnGameEnd(false));
        }

        public void OnEnemyTriggered(EnemyFovInfo info)
        {
            //if sequence is playing - return?
            if (_enemyAnimationSequence.IsActive())
            {
                return;
            }

            //lets perform an enemy appear and eye cast sequence!
            _enemyAnimationSequence = DOTween.Sequence();

            var warningTime = GameSettings.Instance.EnemyWarningTime;

            _enemyAnimationSequence

                //stood up
                .PrependCallback(() => _enemyAnimatorWrapper.PlayRaiseAnimation(info.EnemyRaiseTime))
                .AppendInterval(info.EnemyRaiseTime)
                .AppendCallback(() =>
                {
                    foreach (var particlesTrigger in _eyeParticles)
                    {
                        particlesTrigger.TriggerParticles();
                    }

                    EventBus.RaiseEvent<IEnemyStartGatheringHandler>(h => h.OnStartGathering());
                })
                //warn player for 1.6s
                .AppendInterval(warningTime)
                .AppendCallback((() =>
                {
                    EventBus.RaiseEvent<IEnemySearchingHandler>(h => h.OnEnemySearch(info.EnemyOverallSearchingTime));
                    
                    //cast a fovs with info
                    _enemyVisionManager.TriggerEnemyVision(info, (() =>
                    {
                        //put the animation of PlayerNotFoundHere

                        //dont forget to set animationParam - look at enemyAnimatorWrapper

                        //on end of Player not found, start animation below
                        Sequence endAnimSequence = DOTween.Sequence();

                        endAnimSequence
                            .AppendCallback(() => _enemyAnimatorWrapper.PlayLostAnimation(GameSettings.Instance.EnemyLostTime))
                            .AppendInterval(GameSettings.Instance.EnemyLostTime)
                            .AppendCallback((() => _enemyAnimatorWrapper.PlayHideAnimation(GameSettings.Instance.EnemyHideTime)));
                        
                    }));
                }))
                .SetId(this);
        }

        public void OnGameEnd(bool isWin)
        {
            //return rays back quickly
            QuicklyReturnRays();
            
            if (!isWin)
            {
                _enemyAnimatorWrapper.PlayFindAnimation(GameSettings.Instance.EnemyFindTime);
            }

        }

        void StopDetectingPlayer()
        {
            EventBus.Unsubscribe(this);
            foreach (var eyeFov in _eyesFovs)
            {
                eyeFov.OnTargetDetected -= OnPlayerFound;
            }
        }

        void QuicklyReturnRays()
        {
            _enemyVisionManager.RewindEnemyVision(0.2f);
        }
    }
}
