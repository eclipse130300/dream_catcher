﻿using System;
using System.Collections.Generic;
using DefaultNamespace;
using EventBusSystem;
using EventBusSystem.Handlers;
using UnityEngine;

namespace Enemy
{
    public class EnemyTrigger : TriggerBehaviour<PlayerControllerNew>
    {
        [SerializeField] private float _fovAngle;
        [SerializeField] private float _fovInclination;
        [SerializeField] private float _enemyRaiseTime = 0.3f;
        [SerializeField] private float _raycastDownTime;
        [SerializeField] private float _raycastDownDelay;

        public bool AnimateEyes;
        public float[] AnimatingAngles;

        private void Awake()
        {
            if (_enemyRaiseTime == 0)
            {
                _enemyRaiseTime = 0.01f;
            }
        }

        private void OnValidate()
        {
            if (_enemyRaiseTime == 0)
            {
                _enemyRaiseTime = 0.01f;
            }
        }

        public override void OnBeginInteraction()
        {
            SendEnemyTrigger();
        }

        private void SendEnemyTrigger()
        {
            EnemyFovInfo enemyFovInfo = new EnemyFovInfo();

            enemyFovInfo.FOVAngle = _fovAngle;
            enemyFovInfo.FOVInclination = _fovInclination;
            enemyFovInfo.EnemyRaiseTime = _enemyRaiseTime;
            enemyFovInfo.RaycastDownTime = _raycastDownTime;
            enemyFovInfo.RaycastDownDelay = _raycastDownDelay;
            enemyFovInfo.AnimatingAngles = AnimatingAngles;

            enemyFovInfo.EnemyOverallSearchingTime = _raycastDownTime + _raycastDownDelay;

            EventBus.RaiseEvent<IEnemyTriggeredHandler>(h => h.OnEnemyTriggered(enemyFovInfo));
        }


    }
}
