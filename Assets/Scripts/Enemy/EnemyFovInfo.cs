﻿namespace Enemy
{
    public class EnemyFovInfo
    {
        public float FOVAngle;
        public float FOVInclination;
        public float EnemyRaiseTime;

        public float RaycastDownTime;
        public float RaycastDownDelay;
        public float[] AnimatingAngles;
        
        public float EnemyOverallSearchingTime;
    }
}