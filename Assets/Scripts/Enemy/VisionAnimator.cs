﻿using System;
using DG.Tweening;
using Enemy.Fov;
using EventBusSystem;
using EventBusSystem.Handlers;
using GameSetting;
using UnityEngine;

namespace Enemy
{
    [RequireComponent(typeof(SearchingFieldOfView))]
    public class VisionAnimator : MonoBehaviour
    {
        private SearchingFieldOfView _searchingFieldOfView;
        private Sequence _visionSequence;

        private void Awake()
        {
            _searchingFieldOfView = GetComponent<SearchingFieldOfView>();
        
            //EventBus.Subscribe(this);
        }

        private void OnDestroy()
        {
            //EventBus.Unsubscribe(this);
            DOTween.Kill(this);
        }

        public void AnimateVision(EnemyFovInfo info, Action callback = null)
        {
            DOTween.Kill(this);
        
            _searchingFieldOfView.SetupRaycast(info.FOVAngle, info.FOVInclination);
        
            //createSequence
            _visionSequence = DOTween.Sequence();
        
            //reset length of distance
            _searchingFieldOfView.VisionDistance = 0f;
        
            //get vars for distance from gameSettings
            var maxVisionDistance = GameSettings.Instance.MAXVisionDistance;
            var upTime = GameSettings.Instance.RaycastUpTime;

            //raycast down tween (for each eye)
            Tween downTween = DOTween.To(() => _searchingFieldOfView.VisionDistance, x => _searchingFieldOfView.VisionDistance = x,
                maxVisionDistance, info.RaycastDownTime).SetEase(Ease.InOutQuart);
            //raycast up tween (for each eye)
            Tween upTween = DOTween.To(() => _searchingFieldOfView.VisionDistance, x => _searchingFieldOfView.VisionDistance = x, 0,
                upTime).SetEase(Ease.OutQuart);
            
            //angle animation tween(for each eye)
            Sequence angleSequence = CreateAnmAngleEyesSequence(info);

            _visionSequence
                .Append(downTween)
                .Append(angleSequence)
                .Append(upTween)
                .OnComplete( () => callback?.Invoke()).SetId(this);
            
        }

        public void RewindVision(float time, Action callback = null)
        {
            _visionSequence.Pause();

            DOTween.To(() => _searchingFieldOfView.VisionDistance, x => _searchingFieldOfView.VisionDistance = x, 0,
                time).SetEase(Ease.OutQuart).OnComplete((() => DOTween.Kill(this)));


        }

        private Sequence CreateAnmAngleEyesSequence(EnemyFovInfo info)
        {
            Sequence animationAngleSequence = DOTween.Sequence();

            if (info.AnimatingAngles.Length > 1)
            {
                //foreach angle we specify in Enemy Trigger, create tween and chain it into sequence
                for (int i = 0; i < info.AnimatingAngles.Length; i++)
                {
                    Tween angleTween = DOTween.To(() => _searchingFieldOfView.Inclination,
                        x => _searchingFieldOfView.Inclination = x,
                        _searchingFieldOfView.Inclination + info.AnimatingAngles[i],
                        info.RaycastDownDelay/info.AnimatingAngles.Length).SetEase(Ease.Linear);

                    animationAngleSequence.Append(angleTween);
                }
            }
            else
            {
                animationAngleSequence.AppendInterval(info.RaycastDownDelay);
            }

            return animationAngleSequence;
        }
    }
}
