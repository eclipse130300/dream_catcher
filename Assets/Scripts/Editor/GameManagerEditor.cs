﻿// using System;
// using UnityEditor;
// using UnityEditorInternal;
// using UnityEngine;
//
// namespace Editor
// {
//     [CustomEditor(typeof(GameManager))]
//     public class GameManagerEditor : UnityEditor.Editor
//     {
//         private ReorderableList SceneNamesList;
// 	
//         private void OnEnable() {
//             SceneNamesList = new ReorderableList(serializedObject, 
//                 serializedObject.FindProperty("SceneNames"), 
//                 true, true, true, true);
//             
//             SceneNamesList.drawElementCallback = 
//                 (Rect rect, int index, bool isActive, bool isFocused) => {
//                     var element = SceneNamesList.serializedProperty.GetArrayElementAtIndex(index);
//                     rect.y += 2;
//                     EditorGUI.LabelField(new Rect( rect.x, rect.y, 80, EditorGUIUtility.singleLineHeight), "Level Name: ");
//                     EditorGUI.PropertyField(
//                         new Rect(rect.x + 80, rect.y, rect.width-80, EditorGUIUtility.singleLineHeight),
//                         element, GUIContent.none);
//                 };
//             SceneNamesList.drawHeaderCallback = (Rect rect) => {
//                 EditorGUI.LabelField(rect, "LevelsList");
//             };
//         }
//
//         public override void OnInspectorGUI() {
//             base.OnInspectorGUI();
//             serializedObject.Update();
//             SceneNamesList.DoLayoutList();
//             serializedObject.ApplyModifiedProperties();
//         }
//     }
// }