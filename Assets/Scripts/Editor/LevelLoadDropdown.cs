﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityToolbarExtender;

[InitializeOnLoad]
public class SceneSwitchLeftButton
{
    static SceneSwitchLeftButton()
    {
        ToolbarExtender.LeftToolbarGUI.Add(OnToolbarGUI);
    }

    static void OnToolbarGUI()
    {
        if (EditorApplication.isPlaying)
            return;
        
        GUILayout.FlexibleSpace();

        if(GUILayout.Button(new GUIContent("Default", "Start Scene 1"), ToolbarStyles.DropDownStyle))
        {
            SetScene(true);
        }

        if(GUILayout.Button(new GUIContent("Test", "Start Scene 2"), ToolbarStyles.DropDownStyle))
        {
            SetScene(false);
        }
    }

    private static void SetScene(bool defaultScene)
    {
        var scene = EditorSceneManager.GetActiveScene();

        var roots = scene.GetRootGameObjects();

        foreach (var root in roots)
        {
            var gameSceneLoader = root.GetComponent<GameSceneLoader>();
            if (gameSceneLoader)
            {
                if (defaultScene)
                {
                    gameSceneLoader.FirstLevel = "D1_Main1";
                }
                else
                {
                    gameSceneLoader.FirstLevel = "TestScene";
                }

                EditorUtility.SetDirty(gameSceneLoader);
                EditorSceneManager.MarkSceneDirty(scene);
                EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
                
                
                
                break;
            }
        }
    }
    
    private static class ToolbarStyles
    {
        public static readonly GUIStyle DropDownStyle;
        public static readonly GUIStyle CommandButtonStyle;

        static ToolbarStyles()
        {
            DropDownStyle = new GUIStyle("Command")
            {
                fontSize = 14,
                alignment = TextAnchor.MiddleCenter,
                imagePosition = ImagePosition.ImageAbove,
                fontStyle = FontStyle.Bold,
                fixedWidth = 100f
            };
            
            CommandButtonStyle = new GUIStyle("Command");
        }
    }
}