﻿using Enemy;
using UnityEditor;

namespace Editor
{
    [CustomEditor(typeof(EnemyTrigger))]
    public class EnemyTriggerEditor : UnityEditor.Editor
    {
        private SerializedProperty _animateEyesProperty;
        private SerializedProperty _movingAnglesListProperty;


        private void OnEnable()
        {
            _animateEyesProperty = serializedObject.FindProperty("AnimateEyes");
            _movingAnglesListProperty = serializedObject.FindProperty("AnimatingAngles");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.UpdateIfRequiredOrScript();
            
            DrawPropertiesExcluding(serializedObject, new [] {"AnimateEyes" , "AnimatingAngles"});
            
            // Create a space to separate this enum popup from the other variables 
            EditorGUILayout.Space();

            EditorGUILayout.PropertyField(_animateEyesProperty);

            if (_animateEyesProperty.boolValue)
            {
                EditorGUILayout.PropertyField(_movingAnglesListProperty);
            }
            else
            {
                _movingAnglesListProperty.ClearArray();
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}