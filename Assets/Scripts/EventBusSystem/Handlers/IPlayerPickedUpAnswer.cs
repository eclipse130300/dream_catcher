﻿namespace EventBusSystem.Handlers
{
    public interface IPlayerPickedUpAnswer : IGlobalSubscriber
    {
        void OnPlayerPickedUpAnswer(string answer, int index, string nextGameSceneName = "");
    }
}