﻿namespace EventBusSystem.Handlers
{
    public interface IGameEndHandler : IGlobalSubscriber
    {
        //just over - win or lose dont care
        void OnGameEnd(bool isWin);
    }
}