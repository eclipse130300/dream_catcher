﻿using Enemy;

namespace EventBusSystem.Handlers
{
    public interface IEnemyTriggeredHandler : IGlobalSubscriber
    {
        void OnEnemyTriggered(EnemyFovInfo info);
    }
}