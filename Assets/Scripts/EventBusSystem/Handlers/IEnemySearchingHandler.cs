﻿namespace EventBusSystem.Handlers
{
    public interface IEnemySearchingHandler : IGlobalSubscriber
    {
        void OnEnemySearch(float enemySearchDuration);
    }
}