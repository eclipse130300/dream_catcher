﻿namespace EventBusSystem.Handlers
{
    public interface IEnemyStartGatheringHandler : IGlobalSubscriber
    {
        void OnStartGathering();
    }
}