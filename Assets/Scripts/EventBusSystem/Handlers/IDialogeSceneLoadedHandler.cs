﻿namespace EventBusSystem.Handlers
{
    public interface IDialogeSceneLoadedHandler : IGlobalSubscriber
    {
        void OnDialogeSceneLoaded();
    }
}