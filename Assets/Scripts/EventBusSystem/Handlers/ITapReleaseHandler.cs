﻿namespace EventBusSystem.Handlers
{
    public interface ITapReleaseHandler : IGlobalSubscriber
    {
        void OnTapRelease();
    }
}