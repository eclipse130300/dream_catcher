﻿using Enums;

namespace EventBusSystem.Handlers
{
    public interface IGameUIEventTriggered : IGlobalSubscriber
    {
        void OnUIEventTriggered(UIEventType type);

    }
}