﻿namespace EventBusSystem.Handlers.UI
{
    public interface IPlayerSpeaksHandler : IGlobalSubscriber
    {
        void OnPlayerSpeak();
    }
}