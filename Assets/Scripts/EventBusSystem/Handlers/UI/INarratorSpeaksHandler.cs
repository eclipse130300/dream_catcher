﻿namespace EventBusSystem.Handlers.UI
{
    public interface INarratorSpeaksHandler : IGlobalSubscriber
    {
        void OnNarratorSpeak();
    }
}