﻿namespace EventBusSystem.Handlers
{
    public interface IRunSceneLoadedHandler : IGlobalSubscriber
    {
        void OnRunSceneLoaded();
    }
}