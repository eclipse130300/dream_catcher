﻿namespace EventBusSystem.Handlers
{
    public interface IGameStartedHandler : IGlobalSubscriber
    {
        void OnGameStart();
    }
}