﻿namespace EventBusSystem.Handlers
{
    public interface ITapHandler : IGlobalSubscriber
    {
        void OnTap();
    }
}