﻿using UnityEngine;

namespace GameSetting
{
    public class GameSettings : GameSettingsBase<GameSettings>
    {
        [Header("Enemy vision")] [SerializeField]
        private float _enemyWarningTime = 0.5f;

        [SerializeField] private float _raycastUpTime = 0.2f;

        [SerializeField] private float _enemyHideTime = 0.2f;

        [SerializeField] private float _maxVisionDistance = 10f;

        [SerializeField] private int _eyeRaycount = 100;

        [SerializeField] private float _findanimTime = 20f;

        [SerializeField] private float _lostainmTime = 3f;

        //[SerializeField] private float _reloadDelay;

        [Header("Narration")] 
        [SerializeField] private string _playerDefaultName;

        [SerializeField] private float _dialogueNodesDelay = 0.5f;

        public float EnemyWarningTime => _enemyWarningTime;
        public float RaycastUpTime => _raycastUpTime;
        public float EnemyHideTime => _enemyHideTime;
        public float MAXVisionDistance => _maxVisionDistance;
        public float EnemyFindTime => _findanimTime;
        public float EnemyLostTime => _lostainmTime;

        public int EyeRaycount => _eyeRaycount;
        //public float ReloadDelay => _reloadDelay;
        public string PlayerDefaultName => _playerDefaultName;


        public float DialogueNodesDelay => _dialogueNodesDelay;
        
        [Header("Wind")] 
        [SerializeField] private Color _windColor = new Color(1f,1f,1f,0.5f);

        [Header("Sound")] 
        [SerializeField] private AudioClip[] _playerReplies;

        public AudioClip[] PlayerReplies => _playerReplies;

        [SerializeField] private AudioClip[] _narratorReplies;
        public AudioClip[] NarratorReplies => _narratorReplies;
    }
}