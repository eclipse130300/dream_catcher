﻿#if UNITY_EDITOR
using System;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace GameSetting
{
    public partial class GameSettingsBase
    {
        [MenuItem("Assets/Create/Game Settings", false, 1)]
        private static void CreateInstanceMenu()
        {
            var instance = GetInstance(false);
            if (instance)
            {
                //Debug.LogError($"GameSettings asset already exists at path: {AssetDatabase.GetAssetPath(instance)}");
                EditorUtility.DisplayDialog(
                    "Create GameSettings",
                    $"GameSettings asset already exists at path: {AssetDatabase.GetAssetPath(instance)}",
                    "OK");

                Selection.activeObject = instance;
                return;
            }

            var path = GetCurrentAssetPath();
            if (path == null)
                return;

            var type = GetGameSettingsType(true);
            if (type == null)
                return;

            var asset = CreateInstance(type);
            AssetDatabase.CreateAsset(asset, $"{path}/{AssetName}.asset");
            AssetDatabase.SaveAssets();
            Selection.activeObject = asset;
        }

        [MenuItem("Assets/Create/HyperTools/Game Settings", true)]
        private static bool CreateInstanceMenuValidator()
        {
            return !GetInstance(false);
        }

        private static Type GetGameSettingsType(bool interactive)
        {
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(asm => asm.DefinedTypes)
                .Where(IsGameSettingsType)
                .ToList();

            if (types.Count == 0)
            {
                if (!interactive)
                    return typeof(GameSettingsBase);

                //Debug.LogWarning("Failed to find GameSettings class in the project. Default HyperGameSettings will be used.");
                var rs = EditorUtility.DisplayDialog(
                    "Create GameSettings",
                    "Failed to find GameSettings class in the project.\nDo you want to create a default asset?",
                    "Yes", "No");

                return rs
                    ? typeof(GameSettingsBase)
                    : null;
            }

            if (types.Count > 1)
            {
                if (!interactive)
                    throw new InvalidOperationException("There are many GameSettings classes in the project.");

                EditorUtility.DisplayDialog(
                    "Create GameSettings",
                    "There are many GameSettings classes in the project.",
                    "OK");

                return null;
            }

            return types[0];
        }

        private static GameSettingsBase CreateInstanceAsset()
        {
            var path = GetDefaultAssetPath();
            var type = GetGameSettingsType(false);

            var asset = CreateInstance(type) as GameSettingsBase;
            AssetDatabase.CreateAsset(asset, $"{path}/{AssetName}.asset");
            AssetDatabase.SaveAssets();

            return asset;
        }

        private static bool IsGameSettingsType(Type type)
        {
            if (!type.IsClass || type.IsAbstract || type.IsGenericType)
                return false;
            if (type == typeof(GameSettingsBase))
                return false;

            return typeof(GameSettingsBase).IsAssignableFrom(type);
        }

        private static string GetCurrentAssetPath()
        {
            var selectedObject = Selection.activeObject;
            if (!selectedObject)
            {
                Debug.LogWarning("Failed to get current path in Project View.");
                return null;
            }

            var path = AssetDatabase.GetAssetPath(selectedObject);
            if (!(selectedObject is DefaultAsset))
                path = Path.GetDirectoryName(path);

            var inResources = path.Split('/', '\\').Contains("Resources", StringComparer.OrdinalIgnoreCase);
            if (!inResources)
            {
                EditorUtility.DisplayDialog(
                    "Create GameSettings",
                    "GameSettings asset should be created in Resources folder.",
                    "OK");
                return null;
            }

            return path;
        }

        private static string GetDefaultAssetPath()
        {
            string[] paths = {"Assets/Project/Resources", "Assets/Resources"};

            // get existed path
            foreach (var path in paths)
            {
                if (AssetDatabase.IsValidFolder(path))
                    return path;
            }

            // create one
            AssetDatabase.CreateFolder("Assets", "Resources");
            return "Assets/Resources";
        }
    }
}
#endif