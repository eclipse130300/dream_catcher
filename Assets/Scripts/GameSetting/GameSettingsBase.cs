﻿using UnityEditor;
using UnityEngine;

namespace GameSetting
{
    public partial class GameSettingsBase : ScriptableObject
    {
        private const string AssetName = "GameSettings";

        protected virtual void Reset()
        {
            MarkChangedSafety();
        }

        public void MarkChangedSafety()
        {
#if UNITY_EDITOR
            EditorUtility.SetDirty(this);
#endif
        }

        #region Instancing

        private static GameSettingsBase _instance;

        public static GameSettingsBase Instance => GetInstance(true);

        internal static GameSettingsBase GetInstance(bool createIfNotExists)
        {
            if (_instance)
                return _instance;

            _instance = Resources.Load<GameSettingsBase>(AssetName);
            if (!_instance && createIfNotExists)
            {
#if UNITY_EDITOR
                _instance = Application.isPlaying
                    ? CreateInstance<GameSettingsBase>()
                    : CreateInstanceAsset();
#else
                _instance = CreateInstance<GameSettingsBase>();
#endif
            }

            return _instance;
        }

        #endregion
    }

    public abstract class GameSettingsBase<T> : GameSettingsBase where T : GameSettingsBase<T>
    {
        public new static T Instance => GameSettingsBase.Instance as T;
    }
}