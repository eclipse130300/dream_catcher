﻿using System;
using DefaultNamespace.Utils;
using DG.Tweening;
using UnityEngine;

namespace Level.FrontObstacle
{
    public class FrontObstacleContorller : Singleton<FrontObstacleContorller>
    {
        [SerializeField] private GameObject _frontObstacleRoot;
        [SerializeField] private SpriteRenderer _spriteRenderer;
        [SerializeField] private Camera _mainCam;

        private void Awake()
        {
            _frontObstacleRoot.SetActive(false);
        }

#if UNITY_EDITOR
        private void OnValidate()
        {
            _frontObstacleRoot.SetActive(false);
        }

#endif

        public void ShowFrontObstacle(float timeUp, float stayTime, float timeDown)
        {
            float bottom = -_mainCam.orthographicSize;
            float spriteHeight = _spriteRenderer.size.y;

            Debug.Log(spriteHeight);

            float startY = bottom - spriteHeight;

            Sequence sequence = DOTween.Sequence();

            _frontObstacleRoot.transform.localPosition = Vector3.forward;
            _frontObstacleRoot.transform.SetLocalY(startY);
            _frontObstacleRoot.gameObject.SetActive(true);

            var upTween = 
                _frontObstacleRoot.transform.DOLocalMoveY(bottom, timeUp);
            
            var downTween =
                _frontObstacleRoot.transform.DOLocalMoveY(startY, timeDown);

            sequence.Append(upTween).AppendInterval(stayTime).Append(downTween).OnComplete((() =>
            {
                _frontObstacleRoot.transform.localPosition =  Vector3.forward;
                _frontObstacleRoot.gameObject.SetActive(false);
            }));
        }
    }
}