﻿using System;
using DG.Tweening;
using UnityEngine;

namespace Level.Activations
{
    public class ApeearingObstacleActivation : ActivatableBase
    {
        [SerializeField] private float _apeearDuration = 1f;
        
        [SerializeField] private SpriteRenderer _renderer;
        [SerializeField] private Collider2D _collider;
        
        private static readonly int Amount = Shader.PropertyToID("Amount");

        private void Awake()
        {
            _renderer.material.SetFloat(Amount , 0);
            _collider.enabled = false;
        }

        public override void Activate()
        {
            _renderer.material.DOFloat(1, Amount, _apeearDuration);
            _collider.enabled = true;
        }
    }
}