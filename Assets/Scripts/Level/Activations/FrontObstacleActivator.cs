﻿using Level.FrontObstacle;
using UnityEngine;

namespace Level.Activations
{
    public class FrontObstacleActivator : ActivatableBase
    {
        [SerializeField] private float _upTime = 1f;
        [SerializeField] private float _stayTime = 1f;
        [SerializeField] private float _downTime = 1f;
        public override void Activate()
        {
            FrontObstacleContorller.Instance.ShowFrontObstacle(_upTime, _stayTime, _downTime);
        }
    }
}