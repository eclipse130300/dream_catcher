﻿using EventBusSystem;
using EventBusSystem.Handlers;
using UnityEngine;

namespace Level
{
    public class Finish : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            //var player = other.gameObject.GetComponent<Player.PlayerController>();
            if  (other.gameObject.CompareTag("Player"))
            {
                EventBus.RaiseEvent<IGameEndHandler>(h => h.OnGameEnd(true));
            }
        }
       
    }
}
