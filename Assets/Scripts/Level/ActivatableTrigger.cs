﻿using DefaultNamespace;
using UnityEngine;

namespace Level
{
    public class ActivatableTrigger : TriggerBehaviour<PlayerControllerNew>
    {
        [SerializeField] private ActivatableBase[] _activatables;

        public override void OnBeginInteraction()
        {
            foreach (var activatable in _activatables)
            {
                activatable.Activate();
            }
        }
    }
}