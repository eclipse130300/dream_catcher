﻿using UnityEngine;

namespace Level
{
    public abstract class ActivatableBase : MonoBehaviour
    {
        public abstract void Activate();
    }
}