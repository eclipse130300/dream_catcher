﻿using System;
using DefaultNamespace.Utils;
using Level.Wind;
using UnityEngine;

public class WindController : Singleton<WindController>
{
    [SerializeField] private ParticleSystem[] windParticleSystems;

    private void Awake()
    {
        if (windParticleSystems.Length == 0)
        {
            Debug.LogError("You have not speciefied particle systems yet!");
        }
    }

    public void StartBlowing(WindDirection windDirection, float speed)
    {
        foreach (var system in windParticleSystems)
        {
            var main = system.main;
            
            main.startSpeed = new ParticleSystem.MinMaxCurve((int)windDirection * speed);

            system.Play();
        }
    }

    public void StopBlowing()
    {
        foreach (var system in windParticleSystems)
        {
            system.Stop();
        }
    }
}
