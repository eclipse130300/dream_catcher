﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleXaxisFollow : MonoBehaviour
{
    [SerializeField] private Transform _followObject;

    private Vector3 _startPos;

    private void Start()
    {
        _startPos = transform.position;
    }

    private void LateUpdate()
    {
        var xPos = _followObject.transform.position.x;
        transform.position = new Vector3(xPos, _startPos.y,_startPos.z);
    }
}
