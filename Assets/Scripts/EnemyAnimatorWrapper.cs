﻿using EventBusSystem;
using EventBusSystem.Handlers;
using UnityEngine;

public class EnemyAnimatorWrapper : MonoBehaviour, IGameStartedHandler
{
    private Animator _animator;
    
    private static readonly int RiseUp = Animator.StringToHash("RiseUp");
    private static readonly int Hide = Animator.StringToHash("Hide");
    private static readonly int Find = Animator.StringToHash("Find");
    private static readonly int Lost = Animator.StringToHash("Lost");


    private static readonly int RaiseSpeed = Animator.StringToHash("RaiseSpeed");
    private static readonly int HideSpeed = Animator.StringToHash("HideSpeed");
    private static readonly int FindSpeed = Animator.StringToHash("FindSpeed");
    private static readonly int LostSpeed = Animator.StringToHash("LostSpeed");

    private void Start()
    {
        _animator = GetComponentInChildren<Animator>();

        EventBus.Subscribe(this);
    }

    void OnDestroy()
    {
        EventBus.Unsubscribe(this);
    }

    public void OnGameStart()
    {
        EnemyStartAnimation();
    }

    public void PlayRaiseAnimation(float raiseTime)
    {
        var speed = ConvertTimeToSpeed(raiseTime);
        
        _animator.SetFloat(RaiseSpeed, speed);
        _animator.Play(RiseUp);
        
        //Debug.Log("Enemy Raise!");
    }
    
    public void PlayHideAnimation(float hideTime)
    {
        var speed = ConvertTimeToSpeed(hideTime);
        
        _animator.SetFloat(HideSpeed, speed);
        _animator.Play(Hide);
        
        //Debug.Log("Enemy Hide!");
    }

    public void PlayFindAnimation(float findTime)
    {
        var speed = ConvertTimeToSpeed(findTime);

        _animator.SetFloat(FindSpeed, speed);
        _animator.Play(Find);
    }

    public void PlayLostAnimation(float lostTime)
    {
        var speed = ConvertTimeToSpeed(lostTime);

        _animator.SetFloat(LostSpeed, speed);
        _animator.Play(Lost);
    }
    
    //works only if we have anim length of 1 sec!
    private float ConvertTimeToSpeed(float time)
    {
        return 1 / time;
    }

    private void EnemyStartAnimation()
    {
        PlayHideAnimation(0.5f);
        //Debug.Log("Hide enemy?");
    }
}
