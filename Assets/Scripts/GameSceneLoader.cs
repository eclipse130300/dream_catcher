﻿using System;
using DefaultNamespace.Scenes;
using DefaultNamespace.URP.RenderFeatures.ScreenFXcontrollers;
using DefaultNamespace.Utils;
using DG.Tweening;
using Enums;
using EventBusSystem;
using EventBusSystem.Handlers;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

//may be it is not game manager? it is loader?
public class GameSceneLoader : Singleton<GameSceneLoader>
{
    [FormerlySerializedAs("_firstLevel")] [Scene] public string FirstLevel;

    [SerializeField] private bool _firstLevelIsDialogue = true;

    private string currentLevelName;
    public string CurrentLevelName => currentLevelName;

    private string _finishDialogueSceneName;

    private string _nextGameScene;

    private SceneCoroutineLevelLoader _levelLoader;
    private void Awake()
    {
        _levelLoader = GetComponent<SceneCoroutineLevelLoader>();
    }

    public void SetFinishDialogueSceneName(string scene)
    {
        _finishDialogueSceneName = scene;
    }

    public void SetNextGameSceneName(string scene)
    {
        _nextGameScene = scene;
    }
    

    private void Start()
    {
        //we load first level.
        //later (with saves) we load last opened level
        LoadFirstLevel();
    }

    private void LoadFirstLevel()
    {
        if (_firstLevelIsDialogue)
        {
            _levelLoader.LoadSceneAdditive(FirstLevel);
        }
        else
        {
            _levelLoader.LoadScene(FirstLevel);
        }
        
    }

    public void LoadGameLevel(string name)
    {
        currentLevelName = name;
        _levelLoader.LoadScene(currentLevelName);
    }

    public void ReloadLevel()
    {
        var currentScene = SceneManager.GetActiveScene().name;
        LoadGameLevel(currentScene);
    }

    public void LoadFinishDialogueScene()
    {
        //load additively
        _levelLoader.LoadSceneAdditive(_finishDialogueSceneName);
        
    }

    public void LoadNextGameScene()
    {
        currentLevelName = _nextGameScene;
        _levelLoader.LoadScene(_nextGameScene);
    }

    private void LoadCheatPanel()
    {
        if (SceneUtils.SceneIsInBuildSettings("CheatPanel"))
        {
            _levelLoader.LoadSceneAdditive("CheatPanel");
        }
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            LoadCheatPanel();
        }

        if (Input.touchCount >= 3)
        {
            LoadCheatPanel();
        }
    }
}
