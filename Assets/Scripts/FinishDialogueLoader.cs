﻿using System;
using UnityEngine;

namespace DefaultNamespace
{
    public class FinishDialogueLoader : MonoBehaviour
    {
        [SerializeField, Scene] private string _dialogueToLoadOnFinish;
        private void Start()
        {
            GameSceneLoader.Instance.SetFinishDialogueSceneName(_dialogueToLoadOnFinish);
        }
    }
}