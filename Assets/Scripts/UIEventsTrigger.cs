﻿using System.Collections;
using System.Collections.Generic;
using Enums;
using EventBusSystem;
using EventBusSystem.Handlers;
using UnityEngine;

public class UIEventsTrigger : MonoBehaviour
{
    [SerializeField] private UIEventType _type;
    public void TriggerChosenTypeEvent()
    {
        EventBus.RaiseEvent<IGameUIEventTriggered>(h => h.OnUIEventTriggered(_type));
    }
    

}
