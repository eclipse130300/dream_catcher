﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ParticlesTrigger : MonoBehaviour
{
    private List<ParticleSystem> _childParticles = new List<ParticleSystem>();
    private void Awake()
    {
        _childParticles = GetComponentsInChildren<ParticleSystem>().ToList();

        foreach (var particle in _childParticles)
        {
            //little setup
            var particleMain = particle.main;
            particleMain.playOnAwake = false;
        }
    }

    public void TriggerParticles()
    {
        foreach (var particle in _childParticles)
        {
            if (particle != null)
            {
                particle.Play();
            }
        }
    }
}