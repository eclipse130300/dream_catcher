﻿using System;
using UnityEngine;

namespace DefaultNamespace
{
    public abstract class TriggerBehaviour<T> : MonoBehaviour where T : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            var script = other.gameObject.GetComponent<T>();
            if (script)
            {
                OnBeginInteraction();
            }
        }

        private void OnTriggerExit(Collider other)
        {
            var script = other.gameObject.GetComponent<T>();
            if (script)
            {
                OnEndInteraction();
            }
        }

        public virtual void OnBeginInteraction() {}

        public virtual void OnEndInteraction() {}
    }
}