﻿namespace Player.NEW
{
    public interface IPlayerVisitor
    {
        void Visit(PlayerRun playerRun);
    }
}