﻿using System;
using UnityEngine;

namespace Player.NEW
{
    public class PlayerEffectorTrigger : MonoBehaviour
    {
        [SerializeField] private PlayerEffectorBase _playerStopEffector;
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.GetComponent<PlayerControllerNew>())
            {
                other.GetComponent<PlayerControllerNew>().Accept(_playerStopEffector);
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.GetComponent<PlayerControllerNew>())
            {
                other.GetComponent<PlayerControllerNew>().Decline(_playerStopEffector);
            }
        }
    }
}