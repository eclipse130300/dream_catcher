﻿using Level.Wind;
using UnityEngine;

namespace Player.NEW
{
    [CreateAssetMenu(fileName = "PlayerSpeedEffector", menuName = "Zones/PlayerSpeedEffector", order = 0)]
    public class PlayerSpeedEffector : PlayerEffectorBase
    {
        [SerializeField] private float percentSpeedBoost = 30f;
        [Space(5)]
        [SerializeField] private WindDirection _windDirectionOnActivation = WindDirection.Left;
        [SerializeField] private float _windSpeed = 20f;
        private float _boostValueCached = 0;

        private PlayerRun _playerRun;
        public override void Visit(PlayerRun playerRun)
        {
            _playerRun = playerRun;
            
            SetBoostedSpeed();

            playerRun.OnStartRunning += SetBoostedSpeed;
            playerRun.OnStartStanding += OnStop;

            WindController.Instance.StartBlowing(_windDirectionOnActivation, _windSpeed);
        }

        public override void Leave(PlayerRun playerRun)
        {
            _playerRun = null;
            playerRun.CurrentSpeed = playerRun.DefaultSpeed;
            
            playerRun.OnStartRunning -= SetBoostedSpeed;
            playerRun.OnStartStanding -= OnStop;
            
            WindController.Instance.StopBlowing();
        }

        private void SetBoostedSpeed()
        {
            _boostValueCached = CalculatePercentValue(_playerRun.DefaultSpeed);
            var playerVal = _playerRun.DefaultSpeed;
            _playerRun.CurrentSpeed = playerVal + _boostValueCached;
        }

        private void OnStop()
        {
            _playerRun.CurrentSpeed = 0;
        }

        private float CalculatePercentValue(float originalValue)
        {
           return originalValue / 100 * percentSpeedBoost;
        }
    }
}