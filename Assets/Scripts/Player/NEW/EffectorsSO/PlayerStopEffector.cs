﻿using DG.Tweening;
using UnityEngine;

namespace Player.NEW
{
    [CreateAssetMenu(fileName = "PlayerStopEffector", menuName = "Zones/PlayerStopEffector", order = 0)]
    public class PlayerStopEffector : PlayerEffectorBase
    {
        private PlayerRun _playerRun = null;
        [SerializeField] private float minSpeedOnTapRelease = 0;
        [SerializeField] private float timeToMinSpeed = 0.2f;
        
        public override void Visit(PlayerRun playerRun)
        {
            _playerRun = playerRun;

            _playerRun.OnStartStanding += OnStand;
            _playerRun.OnStartRunning += OnRun;
        }

        public override void Leave(PlayerRun playerRun)
        {
            DOTween.Kill(this);
            
            _playerRun.OnStartStanding -= OnStand;
            _playerRun.OnStartRunning -= OnRun;
            
            OnRun();

            _playerRun.StopDirt();
            _playerRun = null;
        }
        private void OnStand()
        {
            _playerRun.PlayerDirt();

            float speedToSet = _playerRun.CurrentSpeed;
            
            DOTween.To(GetSpeed,
                    SetSpeed,
                    minSpeedOnTapRelease,
                    timeToMinSpeed)
                .SetId(this)
                .SetEase(Ease.OutQuad)
                .OnUpdate((() =>
                {
                    _playerRun.CurrentSpeed = speedToSet;

                    var runMain = _playerRun.DirtParticles.main;
                    
                    runMain.startSpeed = new ParticleSystem.MinMaxCurve(speedToSet);
                }))
                .OnComplete((() =>
                {
                    _playerRun.StopDirt();
                }));

            void SetSpeed(float speed)
            {
                speedToSet = speed;
            }

            float GetSpeed()
            {
                return speedToSet;
            }
        }

        private void OnRun()
        {
            DOTween.Kill(this);
            
            _playerRun.CurrentSpeed = _playerRun.DefaultSpeed;
        }
    }
}