﻿using UnityEngine;

namespace Player.NEW
{
    public abstract class PlayerEffectorBase : ScriptableObject, IPlayerVisitor, IPlayerLeaver
    {
        public abstract void Visit(PlayerRun playerRun);

        public abstract void Leave(PlayerRun playerRun);
    }
}