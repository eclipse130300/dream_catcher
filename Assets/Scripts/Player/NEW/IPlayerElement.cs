﻿namespace Player.NEW
{
    public interface IPlayerElement
    {
        void Accept(IPlayerVisitor visitor);

        void Decline(IPlayerLeaver leaver);
    }
}