﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Player.NEW;
using UnityEngine;

public class PlayerControllerNew : MonoBehaviour, IPlayerElement
{
    private List<IPlayerElement> _elements = new List<IPlayerElement>();

    private void Awake()
    {
        _elements = GetComponentsInChildren<IPlayerElement>().ToList();

        if (_elements.Contains(this))
        {
            _elements.Remove(this);
        }
    }

    public void Accept(IPlayerVisitor visitor)
    {
        foreach (var element in _elements)
        {
            element.Accept(visitor);
        }
    }

    public void Decline(IPlayerLeaver leaver)
    {
        foreach (var element in _elements)
        {
            element.Decline(leaver);
        }
    }
}
