﻿using System;
using EventBusSystem;
using EventBusSystem.Handlers;
using UnityEngine;

namespace Player.NEW
{
    public class PlayerRun : MonoBehaviour, IPlayerElement, ITapHandler, ITapReleaseHandler, IGameEndHandler
    {
        [SerializeField] private float defaultSpeed = 5f;
        [SerializeField] private ParticleSystem _dirtParticles;
        [SerializeField] private PlayerAnimator _animator;

        public ParticleSystem DirtParticles => _dirtParticles;

        public float DefaultSpeed => defaultSpeed;
        public float CurrentSpeed { get; set; }

        private bool _isActive;

        public event Action OnStartRunning;
        public event Action OnStartStanding;

        private int visitorsCount = 0;
        private bool IsDefultStateActive => visitorsCount == 0;

        private void Awake()
        {
            EventBus.Subscribe(this);

            CurrentSpeed = 0;
        }

        private void OnDestroy()
        {
            EventBus.Unsubscribe(this);
        }

        public void Accept(IPlayerVisitor visitor)
        {
            visitor.Visit(this);
            visitorsCount++;
            
            //Debug.Log($"accept run, visitors : {visitorsCount}");
        }

        public void Decline(IPlayerLeaver leaver)
        {
            leaver.Leave(this);
            visitorsCount--;
            
            //Debug.Log($"decline run, visitors : {visitorsCount}");
        }

        public void OnTap()
        {
            //first tap
            if (!_isActive)
            {
                CurrentSpeed = defaultSpeed;
                _isActive = true;
                
                EventBus.RaiseEvent<IGameStartedHandler>(h => h.OnGameStart());
                
                OnStartRunning?.Invoke();
                _animator.PlayerRunAnimation();
            }
            else
            {
                OnStartStanding?.Invoke();
                _animator.PlayIdleAnimation();
            }
        }

        public void OnTapRelease()
        {
            OnStartRunning?.Invoke();
            _animator.PlayerRunAnimation();
        }

        public void OnGameEnd(bool isWin)
        {
            CurrentSpeed = 0;
            EventBus.Unsubscribe(this);
            _animator.PlayIdleAnimation();
            _isActive = false;
        }

        private void Update()
        {
            if (_isActive)
            {
                if (IsDefultStateActive)
                {
                    if (Input.GetMouseButton(0))
                    {
                        CurrentSpeed = 0;
                    }
                    else
                    {
                        CurrentSpeed = defaultSpeed;
                    }
                }
            }

            //Debug.Log(CurrentSpeed);
            
            //todo: replace with subcomponent system?
            transform.parent
                .Translate(Vector3.right * 
                           (CurrentSpeed * Time.deltaTime));
        }
        
        
        public void PlayerDirt()
        {
            _dirtParticles.Play();
        }

        public void StopDirt()
        {
            _dirtParticles.Stop();
        }
    }
}