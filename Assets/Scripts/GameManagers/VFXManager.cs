﻿using DG.Tweening;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

public class VFXManager : MonoBehaviour
{
    [SerializeField] private BubbleMaterialSettings _bubbleMaterialSettings;
    private static readonly int OverallDensity = Shader.PropertyToID("_OverallDensity");

    private void Start()
    {
        AnimateSlippyMat();
    }

    private void AnimateSlippyMat()
    {
        var bubbleMat = _bubbleMaterialSettings.BubbleMaterial;
        Sequence sequence = DOTween.Sequence();

        Tween to = bubbleMat.DOFloat(_bubbleMaterialSettings.MaxDensity,
            OverallDensity,
            Random.Range(_bubbleMaterialSettings.DurationMin,
                _bubbleMaterialSettings.DurationMax));

        Tween fro = bubbleMat.DOFloat(_bubbleMaterialSettings.MinDensity,
            OverallDensity,
            Random.Range(_bubbleMaterialSettings.DurationMin,
                _bubbleMaterialSettings.DurationMax));

        var interval = Random.Range(0,
            1.5f);

        sequence.Append(to).AppendInterval(interval).Append(fro).OnComplete((AnimateSlippyMat));
        
    }
    
    [System.Serializable]
    public class BubbleMaterialSettings
    {
        public float MinDensity = 0.5f;
        public float MaxDensity = 0.7f;
        public Material BubbleMaterial;

        public float DurationMin = 0.1f;
        public float DurationMax = 0.3f;
    }
}
