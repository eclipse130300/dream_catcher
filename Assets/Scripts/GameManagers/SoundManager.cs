﻿using System.Collections;
using System.Collections.Generic;
using DefaultNamespace.Utils;
using UnityEngine;

public class SoundManager : Singleton<SoundManager>
{
    [SerializeField] private AudioSource _effectsSource;
    [SerializeField] private AudioSource _musicSource;
    
    public void PlaySound(AudioClip clip)
    {
        _effectsSource.PlayOneShot(clip);
    }

    public void PlayRandomSound(AudioClip[] clips)
    {
        var soundToPlay = clips[Random.Range(0, clips.Length)];
        PlaySound(soundToPlay);
    }

    public void PlayMusic(AudioClip clip)
    {
        _musicSource.Stop();
        _musicSource.clip = clip;
        _musicSource.Play();
    }

    public void StopAllSounds()
    {
        _effectsSource.Stop();
        _musicSource.Stop();
    }

    public void StartLoopForDuration(AudioClip clip, float duration)
    {
        GameObject audioSourceObject = new GameObject();
        audioSourceObject.transform.parent = transform;
        var audioSource = audioSourceObject.AddComponent<AudioSource>();
        audioSource.clip = clip;
        audioSource.loop = true;

        StartCoroutine(StartLoop());
        
        IEnumerator StartLoop()
        {
            audioSource.Play();
            yield return new WaitForSeconds(duration);
            Destroy(audioSourceObject);
        }
    }
}
