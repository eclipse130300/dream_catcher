﻿using EventBusSystem;
using EventBusSystem.Handlers;
using EventBusSystem.Handlers.UI;
using GameSetting;
using UnityEngine;

public class GameStateSoundController : MonoBehaviour,
    IGameEndHandler,
    IGameStartedHandler,
    IRunSceneLoadedHandler,
    IDialogeSceneLoadedHandler,
    IEnemyStartGatheringHandler,
    IEnemySearchingHandler, 
    IPlayerSpeaksHandler,
    INarratorSpeaksHandler
{
    [SerializeField] private AudioClip _gameStartSound;
    [SerializeField] private AudioClip _gameWinSound;
    [SerializeField] private AudioClip _gameLoseSound;
    [SerializeField] private AudioClip _gameLoseSound2;

    [Space(10f)] 
    [SerializeField] private AudioClip _dialogueMusic;
    //[SerializeField] private AudioClip _runMusic;
    
    [Space(10f)]
    [SerializeField] private AudioClip _enemyGatherSound;
    [SerializeField] private AudioClip _enemyBlastSound;
    [SerializeField] private AudioClip _enemySearchingSound;

    [Space(10)] 
    [SerializeField] private AudioClip _playerSpeaksSound;
    [SerializeField] private AudioClip _narratorSpeaksSound;

    private void Awake()
    {
        EventBus.Subscribe(this);
    }

    private void OnDestroy()
    {
        EventBus.Unsubscribe(this);
    }

    public void OnGameEnd(bool isWin)
    {
        SoundManager.Instance.StopAllSounds();
        
        if (isWin)
        {
            SoundManager.Instance.PlaySound(_gameWinSound);
        }
        else
        {
            SoundManager.Instance.PlaySound(_gameLoseSound);
            SoundManager.Instance.PlaySound(_gameLoseSound2);
        }
    }

    public void OnGameStart()
    {
        SoundManager.Instance.PlaySound(_gameStartSound);
    }

    public void OnRunSceneLoaded()
    {
        SoundManager.Instance.StopAllSounds();
        //SoundManager.Instance.PlayMusic(_runMusic);
    }

    public void OnDialogeSceneLoaded()
    {
        SoundManager.Instance.PlayMusic(_dialogueMusic);
    }

    public void OnStartGathering()
    {
        SoundManager.Instance.StartLoopForDuration(_enemyGatherSound, GameSettings.Instance.EnemyWarningTime);
    }

    public void OnEnemySearch(float enemySearchDuration)
    {
        SoundManager.Instance.StartLoopForDuration(_enemySearchingSound, enemySearchDuration);
        SoundManager.Instance.PlaySound(_enemyBlastSound);
    }

    public void OnPlayerSpeak()
    {
        SoundManager.Instance.PlayRandomSound(GameSettings.Instance.PlayerReplies);
    }

    public void OnNarratorSpeak()
    {
        SoundManager.Instance.PlayRandomSound(GameSettings.Instance.NarratorReplies);
    }
}
