﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class AudioClipsPlayer : MonoBehaviour
{
    [SerializeField] private AudioClip[] _stepsClips;

    public void PlayRandomClip()
    {
        PlayClip(true);
    }

    public void PlayFirstClip()
    {
        PlayClip();
    }

    private void PlayClip(bool random = false)
    {
        if (_stepsClips == null || _stepsClips.Length == 0)
        {
#if UNITY_EDITOR
            Debug.LogWarning("Set up clips in audio clips player!");
            Selection.activeObject = transform;
#endif
            return;
        }
        
        AudioClip clip = random
            ? _stepsClips[Random.Range(0, _stepsClips.Length)]
            : _stepsClips[0];
        
        SoundManager.Instance.PlaySound(clip);
    }
}
