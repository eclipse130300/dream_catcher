﻿using System;
using System.Linq;
using DefaultNamespace.Utils;
using EventBusSystem;
using EventBusSystem.Handlers;
using UnityEngine;
using UnityEngine.Rendering.Universal;

namespace DefaultNamespace.URP.RenderFeatures.ScreenFXcontrollers
{
    public abstract class FXController<T> : Singleton<T>, IRunSceneLoadedHandler where T : Component
    {
        [SerializeField] protected ForwardRendererData rendererData = null;
        [SerializeField] protected string featureName = null;

        private void Awake()
        {
            EventBus.Subscribe(this);
        }

        private void OnDestroy()
        {
            EventBus.Subscribe(this);
            ResetTransition();
        }

        public void OnRunSceneLoaded()
        {
            //at the start of each run disable all fx
            EndTransition(true);
        }
        
        protected bool TryGetFeature(out ScriptableRendererFeature feature)
        {
            feature = rendererData.rendererFeatures.FirstOrDefault(f => f.name == featureName);
            return feature != null;
        }

        protected abstract void ResetTransition();

        public abstract void EndTransition(bool force, float time = 0.01f);

        public abstract void TransitionTo(float time = 0.01f);
    }
}