﻿using System.Linq;
using DefaultNamespace.URP.RenderFeatures.ScreenFXcontrollers;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class BlurController : FXController<BlurController>
{
    private static readonly int Amount = Shader.PropertyToID("_Amount");
    
    private void OnDestroy() {
        ResetTransition();
    }

    public override void TransitionTo(float time = 0.01f)
    {
        if (TryGetFeature(out var feature))
        {
            var blitFeature = feature as BlitRenderPass;

            if (blitFeature)
            {
                feature.SetActive(true);
                rendererData.SetDirty();
                
                var material = blitFeature.Material;

                material.DOFloat(1, Amount, time);
            }
        }
    }

    public override void EndTransition(bool force, float time = 0.01f)
    {
        if (TryGetFeature(out var feature))
        {
            var blitFeature = feature as BlitRenderPass;

            if (blitFeature)
            {
                var material = blitFeature.Material;

                if (force)
                {
                    material.SetFloat(Amount, 0);
                    feature.SetActive(false);
                    rendererData.SetDirty();
                }
                else
                {
                    material.DOFloat(0, Amount, time).OnComplete((() =>
                    {
                        feature.SetActive(false);
                        rendererData.SetDirty();
                    })); 
                }
            }
        }
    }

    protected override void ResetTransition() {
        if (TryGetFeature(out var feature))
        {
            var blitFeature = feature as BlitRenderPass;

            if (blitFeature)
            {
                var material = blitFeature.Material;
                material.SetFloat(Amount, 0);
                feature.SetActive(true);
                rendererData.SetDirty();
            }
        }
    }
}
