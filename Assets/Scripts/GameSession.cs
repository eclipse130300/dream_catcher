﻿using System;
using DefaultNamespace.URP.RenderFeatures.ScreenFXcontrollers;
using DG.Tweening;
using Enums;
using EventBusSystem;
using EventBusSystem.Handlers;
using GameSetting;
using UnityEngine;

namespace DefaultNamespace
{
    public class GameSession : MonoBehaviour , IGameUIEventTriggered, IGameEndHandler
    {
        private void Awake()
        {
            //persistent manager gameobject!
            DontDestroyOnLoad(this);
            
            EventBus.Subscribe(this);
        }
        
        private void OnDestroy()
        {
            EventBus.Unsubscribe(this);
        }

        public void OnUIEventTriggered(UIEventType type)
        {
            switch (type)
            {
                case UIEventType.Restart:
                    GameSceneLoader.Instance.ReloadLevel();
                    break;
            
                case UIEventType.PlayNextLevel:
                    GameSceneLoader.Instance.LoadNextGameScene();
                    break;
                
                case UIEventType.LoadDiagloue:
                    OnLoadDialogue();
                    break;
            }
        }

        public void OnGameEnd(bool isWin)
        {
            if (isWin)
            {
                OnGameWin();
            }
            else
            {
                LoseGameSequence();
            }
        }

        private void OnLoadDialogue()
        {
            Sequence sequence = DOTween.Sequence();

            sequence.AppendCallback(() => BlurController.Instance.TransitionTo(1));
            sequence.AppendInterval(1f)
                .OnComplete(() =>
                {
                    GameSceneLoader.Instance.LoadFinishDialogueScene();
                    GameUIManager.Instance.ToggleCanvas(false);
                });
        }

        private void OnGameWin()
        {
            GameUIManager.Instance.ToggleWinText(true);
        }

        private void LoseGameSequence()
        {
            Sequence sequence = DOTween.Sequence();

            //use desaturation here?
            sequence.AppendCallback(() => DesaturationController.Instance.TransitionTo(1));
            sequence.AppendInterval(GameSettings.Instance.EnemyFindTime)
                .OnComplete(() =>
                {
                    
                    GameUIManager.Instance.ToggleLoseText(true);
                });
        }
    }
}