﻿using DG.Tweening;
using UnityEngine;

public class Wobbler : MonoBehaviour
{
    [SerializeField] private float maxScale = 1.15f;
    [SerializeField] private float growTime = 0.5f;
    [SerializeField] private float shrinkTime = 0.5f;

    private void OnEnable()
    {
        Show();
    }

    private void OnDisable()
    {
        DOTween.Kill(this);
    }

    public void Show()
    {
        gameObject.SetActive(true);

        //transform.localScale = Vector3.zero;

        var sequence = DOTween.Sequence();

        Tween bounceUp = transform.DOScale(Vector3.one * maxScale, growTime);
        Tween bounceBackToOne = transform.DOScale(Vector3.one, shrinkTime);

        sequence
            .Append(bounceUp)
            .Append(bounceBackToOne)
            .SetId(this).SetLoops(-1);
    }
}
