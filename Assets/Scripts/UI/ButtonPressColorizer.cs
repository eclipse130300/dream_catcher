﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace DefaultNamespace.UI
{
    public class ButtonPressColorizer : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        //we can also add images here or smthing!
        [SerializeField] private TextMeshProUGUI _textToColorize;
        
        [SerializeField] private Color _inactiveColor;
        [SerializeField] private Color _activeColor;

        private void Awake()
        {
            _textToColorize.color = _inactiveColor;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            _textToColorize.color = _activeColor;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            _textToColorize.color = _inactiveColor;
        }
    }
}