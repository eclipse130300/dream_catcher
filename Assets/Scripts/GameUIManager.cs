﻿using UnityEngine;
using System;
using DefaultNamespace.Utils;
using EventBusSystem;
using EventBusSystem.Handlers;
using TMPro;

public class GameUIManager : Singleton<GameUIManager>, ITapHandler//, IGameEndHandler
{
    [SerializeField] private RectTransform _youWinObject;
    [SerializeField] private RectTransform _startObject;
    [SerializeField] private RectTransform _youLoseObject;
    [SerializeField] private TextMeshProUGUI _levelText;

    private void Awake()
    {
        ShowStartText();
        EventBus.Subscribe(this);
    }

    private void Start()
    {
        _levelText.text = GameSceneLoader.Instance.CurrentLevelName;
    }

    void OnDestroy()
    {
        EventBus.Unsubscribe(this);
    }
    
    public void ToggleLoseText(bool isOn)
    {
        _youLoseObject.gameObject.SetActive(isOn);
    }

    public void ToggleWinText(bool isOn)
    {
        _youWinObject.gameObject.SetActive(isOn);
    }

    public void ToggleCanvas(bool isOn)
    {
        gameObject.SetActive(isOn);
    }

    private void ShowStartText()
    {
        _startObject.gameObject.SetActive(true);
    }

    private void HideStartText()
    {
        _startObject.gameObject.SetActive(false);
    }

    public void OnTap()
    {
        HideStartText();
    }

    // public void OnGameEnd(bool isWin)
    // {
    //     if (isWin)
    //     {
    //         ToggleWinText();
    //     }
    //     else
    //     {
    //         ShowLoseText();
    //     }
    // }
}
