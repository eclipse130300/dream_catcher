﻿using UnityEngine;
 
public class VFXSortOrder : MonoBehaviour
{
    public int sortingOrder = 5;
    public Renderer vfxRenderer;
    public string LayerName = "Player";
    void OnValidate()
    {
        vfxRenderer = GetComponent<Renderer>();
        if (vfxRenderer)
        {
            vfxRenderer.sortingOrder = sortingOrder;
            vfxRenderer.sortingLayerName = LayerName;
        }
    }
}
